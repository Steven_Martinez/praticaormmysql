'use strict';
module.exports = (sequelize, DataTypes) => {
    const historia = sequelize.define('historia', {
        external_id: DataTypes.UUID,
        nro_historia: DataTypes.STRING,
        contacto: DataTypes.STRING,
        enfermedades: DataTypes.STRING,
        enfer_hed: DataTypes.STRING,
        habitos: DataTypes.STRING
    }, {freezeTableName: true});
    historia.associate = function (models) {
        // associations can be defined here}
        historia.belongsTo(models.paciente, {foreignKey: 'id_paciente'});
        historia.hasMany(models.consulta, {foreignKey: 'id_historia', as: 'consultas'});
    };
    return historia;
};