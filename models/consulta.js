'use strict';
module.exports = (sequelize, DataTypes) => {
    const consulta = sequelize.define('consulta', {
        external_id: DataTypes.UUID,
        diagnostico: DataTypes.STRING,
        fecha: DataTypes.DATEONLY,
        motivo: DataTypes.STRING,
        receta: DataTypes.STRING
    }, {freezeTableName: true});
    consulta.associate = function (models) {
        // associations can be defined here
        consulta.belongsTo(models.persona, {foreignKey: 'id_persona', as: 'medico'});
        consulta.belongsTo(models.historia, {foreignKey: 'id_historia', as: 'historia'});
    };
    return consulta;
};