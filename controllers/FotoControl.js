'use strict';
var models = require('./../models/');
var persona = models.persona;
class FotoControl {
    visualizar(req, res) {
        persona.findOne({where: {external_id: req.user.external}}).then(function (result) {
            if (result) {
                res.render('index', {title: "Principal",
                    fragmento: 'fragmentos/foto/frm_foto',
                    sesion: true,
                    persona: result
                });
            } else {

            }
        });
    }

}

module.exports = FotoControl;

