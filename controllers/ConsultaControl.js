'use strict';
var models = require('./../models/');
var consultaC = models.consulta;
var uuid = require('uuid');
var historiaC = models.historia;
var medicoC = models.persona;
class ConsultaControl {

    visualizar(req, res) {
        consultaC.findAll({include: [{model: models.historia, as: 'historia', include: [{model: models.paciente, as: 'paciente'}]}, {model: models.persona, as: 'medico'}]}).then(function (listaC) {
            res.render('index', {
                title: 'Lista Consultas',
                fragmento: 'fragmentos/consulta/frm_consulta',
                sesion: true,
                consulta: listaC,
                medico: req.user,
                msg: {error: req.flash('error'), info: req.flash('info')}
            });
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect('/');
        });
    }

    guardar(req, res) {
        medicoC.findAll({where: {external_id: req.body.medico}}).then(function (data) {
            if (data.length > 0) {
                var medico = data[0];
                console.log("Medico cd: " + medico.id);
                historiaC.findAll({where: {external_id: req.body.historia}, include: [{model: models.paciente, as: 'paciente'}]}).then(function (dataP) {
                    if (dataP.length > 0) {
                        var historia = dataP[0];
                        console.log("Historia cd: " + historia.id);
                        var dataC = {
                            external_id: uuid.v4(),
                            diagnostico: req.body.diagnostico,
                            fecha: req.body.fecha,
                            motivo: req.body.motivo,
                            receta: req.body.receta,
                            id_persona: medico.id,
                            id_historia: historia.id
                        };
                        consultaC.create(dataC).then(function (save) {
                            req.flash('info', 'Se ha guardado exitosamente!');
                            res.redirect('/administracion/consultas');
                        }).error(function (error) {
                            req.flash('error', 'No se guardo la consulta');
                            res.redirect('/administracion/consultas');
                        });
                    } else {
                        req.flash('error', 'No se encontro la historia!');
                        res.redirect('/administracion/consultas');
                    }
                }).error(function (error) {
                    req.flash('error', 'Hubo un error en historia!');
                    res.redirect('/administracion/consultas');
                });
            } else {
                req.flash('error', 'No se encontro el medico!');
                res.redirect('/administracion/consultas');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un error en medico!');
            res.redirect('/administracion/consultas');
        });
    }

    visualizarModificar(req, res) {
        var external = req.params.external;
        consultaC.findAll({where: {external_id: external}, include: [{model: models.historia, as: 'historia'}, {model: models.persona, as: 'medico'}]}).then(function (data) {
            if (data.length > 0) {
                var consulta = data[0];
                res.render('index',
                        {title: 'Modificar Consulta',
                            fragmento: "fragmentos/consulta/frm_modificar_consulta",
                            sesion: true,
                            consulta: consulta,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            } else {
                req.flash('error', 'No se pudo encontrar lo solicitado');
                res.redirect('/administracion/consultas');
            }
        }).error(function (error) {
            req.flash('error', 'No se pudo encontrar lo solicitado');
            res.redirect('/');
        });
    }

    modificar(req, res) {
        consultaC.findAll({where: {external_id: req.body.external}, include: [{model: models.historia, as: 'historia'}, {model: models.persona, as: 'medico'}]}).then(function (save) {
            if (save.length > 0) {
                var consulta = save[0];
                var medico = consulta.id_persona;
                var historia = consulta.id_historia;
                console.log("MEdico id: " + medico);
                console.log("Historia id: " + historia);
                consulta.diagnostico = req.body.diagnostico;
                consulta.motivo = req.body.motivo;
                consulta.receta = req.body.receta;
                consulta.id_persona = medico;
                consulta.id_historia = historia;
                consulta.save().then(function (save) {
                    req.flash('info', 'Modificado!');
                    res.redirect('/administracion/consultas');
                }).error(function () {
                    req.flash('error', 'No se pudo modificar!');
                    res.redirect('/administracion/consultas');
                });
            } else {
                req.flash('error', 'No se encuentra consulta!');
                res.redirect('/administracion/consultas');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un problema');
            res.redirect('/administracion/consultas');
        });
    }

}
module.exports = ConsultaControl;

