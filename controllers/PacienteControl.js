'use strict';
var models = require('./../models/');
var uuid = require('uuid');
var pacienteC = models.paciente;

class PacienteControl {
    /**
     * Visualizar lista los paciente existentes en la BD
     * inlude une los modelos paciente-historia
     * render envia una lista de pacientes
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizar(req, res) {
        pacienteC.findAll({include: [{model: models.historia, as: 'historia'}]}).then(function (listaP) {
            var nro = "NHIS-" + (listaP.length + 1);
            console.log("Pacientes: " + listaP);
            res.render('index', {
                title: 'Lista Pacientes',
                fragmento: 'fragmentos/paciente/frm_tabla_paciente',
                lista: listaP,
                sesion: true,
                nro: nro,
                msg: {error: req.flash('error'), info: req.flash('info')}
            });
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect('/');
        });
    }

    /**
     * Funcion guardar envia la data del paciente incluyendo la data de historia
     * La guarda x el metodo create
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    guardar(req, res) {
        var dataP = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            direccion: req.body.direccion,
            external_id: uuid.v4(),
            historia: {
                nro_historia: req.body.nro_his,
                enfermedades: req.body.enf,
                enfer_hed: req.body.enf_her,
                habitos: req.body.hab,
                contacto: req.body.contacto,
                external_id: uuid.v4()
            }
        };
        pacienteC.create(dataP, {include: [{model: models.historia, as: 'historia'}]}).then(function (save) {
            req.flash('info', 'Se ha guardado exitosamente!');
            res.redirect('/administracion/pacientes');
        }).error(function (error) {
            req.flash('error', 'No se guardo el paciente');
            res.redirect('/administracion/pacientes');
        });
    }
    /**
     * Obtiene el paciente x external_id 
     * recogido x params
     * include historia
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    visualizarModificar(req, res) {
        var external = req.params.external;
        console.log("External: " + external);
        pacienteC.findAll({where: {external_id: external}, include: [{model: models.historia, as: 'historia'}]}).then(function (data) {
            if (data.length > 0) {
                var paciente = data[0];
                console.log("Modificar: " + paciente);
                res.render('index',
                        {title: 'Modificar Paciente',
                            fragmento: "fragmentos/paciente/frm_modificar-paciente",
                            sesion: true,
                            paci: paciente,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            } else {
                req.flash('error', 'No se pudo encontrar el paciente');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un problema');
            res.redirect('/administracion/pacientes');
        });
    }
    /**
     * Modifica el paciente y la historia 
     * con el metodo save 
     * paciente.historia relaciona las tablas 
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    modificar(req, res) {
        pacienteC.findAll({where: {external_id: req.body.external}, include: [{model: models.historia, as: 'historia'}]}).then(function (save) {
            if (save.length > 0) {
                var paciente = save[0];
                paciente.apellidos = req.body.apellidos;
                paciente.nombres = req.body.nombres;
                paciente.edad = req.body.edad;
                paciente.direccion = req.body.direccion;
                paciente.fecha_nac = req.body.fecha_nac;
                paciente.save().then(function (save) {
                    var historia = paciente.historia;
                    historia.enfermedades = req.body.enf;
                    historia.enfer_hed = req.body.enf_her;
                    historia.habitos = req.body.hab;
                    historia.contacto = req.body.contacto;
                    historia.save();
                    req.flash('info', 'Modificado!');
                    res.redirect('/administracion/pacientes');
                }).error(function () {
                    req.flash('error', 'No se pudo modificar!');
                    res.redirect('/administracion/pacientes');
                });
            } else {
                req.flash('error', 'No se encuentra paciente!');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un problema');
            res.redirect('/administracion/pacientes');
        });
    }

    buscar(req, res) {
        var texto = req.query.texto;
        var data = {};
        pacienteC.findAll({where: {cedula: texto}, include: [{model: models.historia, as: 'historia'}]}).then(function (lista) {
            if (lista.length > 0) {
                lista.forEach(function (item, index) {
                    data[index] = {
                        //Paciente
                        external: item.external_id,
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        fecha_nac: item.fecha_nac,
                        edad: item.edad,
                        direccion: item.direccion,
                        //Historia
                        nro: item.historia.nro_historia,
                        enfermedades: item.historia.enfermedades,
                        enfer_hed: item.historia.enfer_hed,
                        habitos: item.historia.habitos,
                        contacto: item.historia.contacto,
                        external_h: item.historia.external_id
                    };
                });
                res.json(data);
            } else {
                req.flash('error', 'no existe el paciente buscado!');
                res.redirect('/administracion/consultas');
            }
        }).error(function () {
            req.flash('error', 'Se ha producido un error!');
            res.redirect('/administracion/consultas');
        });

    }
}

module.exports = PacienteControl;

