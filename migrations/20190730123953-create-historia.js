'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('historia', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      external_id: {
        type: Sequelize.UUID
      },
      nro_historia: {
        type: Sequelize.STRING
      },
      contacto: {
        type: Sequelize.STRING
      },
      enfermedades: {
        type: Sequelize.STRING
      },
      enfer_hed: {
        type: Sequelize.STRING
      },
      habitos: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('historia');
  }
};