'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('paciente', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            external_id: {
                type: Sequelize.UUID
            },
            cedula: {
                type: Sequelize.STRING
            },
            apellidos: {
                type: Sequelize.STRING
            },
            nombres: {
                type: Sequelize.STRING
            },
            fecha_nac: {
                type: Sequelize.DATEONLY
            },
            edad: {
                type: Sequelize.INTEGER
            },
            direccion: {
                type: Sequelize.STRING
            },
            foto: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('pacientes');
    }
};