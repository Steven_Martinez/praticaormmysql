'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
                'cuenta',
                'id_persona',
                {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'persona',
                        key: 'id'
                    },
                    onUpdate: 'CASCADE',
                    onDelete: 'SET NULL'
                }).then(() => {
            return queryInterface.addColumn('historia', 'id_paciente', {
                type: Sequelize.INTEGER,
                references: {
                    model: 'paciente',
                    key: 'id'
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL'
            }).then(() => {
                return queryInterface.addColumn('consulta', 'id_persona', {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'persona',
                        key: 'id'
                    },
                    onUpdate: 'CASCADE',
                    onDelete: 'SET NULL'
                }).then(() => {
                    return queryInterface.addColumn('consulta', 'id_historia', {
                        type: Sequelize.INTEGER,
                        references: {
                            model: 'historia',
                            key: 'id'
                        },
                        onUpdate: 'CASCADE',
                        onDelete: 'SET NULL'
                    }).then(() => {

                    });
                });
            });
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('cuenta', 'id_persona');
        return queryInterface.removeColumn('historia', 'id_paciente');
        return queryInterface.removeColumn('consulta', 'id_persona');
        return queryInterface.removeColumn('consulta', 'id_historia');
    }
};
