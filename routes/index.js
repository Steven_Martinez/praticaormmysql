var express = require('express');
var router = express.Router();
var passport = require('passport');
var persona = require('../controllers/PersonaControl');
var personaC = new persona();
var cuenta = require('../controllers/CuentaControl');
var cuentaC = new cuenta();
//Paciente
var paciente = require('../controllers/PacienteControl');
var pacienteC = new paciente();
//Consulta
var consulta = require('../controllers/ConsultaControl');
var consultaC = new consulta();
//Perfil
var foto = require('../controllers/FotoControl');
var fotoC = new foto();

var auth = function (req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        req.flash('error', "Debe iniciar sesion primero");
        res.redirect("/");
    }
}
/* GET home page. */
router.get('/', function (req, res, next) {
    if (req.isAuthenticated()) {
        res.render('index', {title: "Principal",
            fragmento: 'fragmentos/principal/frm_principal',
            sesion: true,
            usuario: req.user.nombre});
    } else {
        res.render('index', {title: 'Medicos',
            msg: {error: req.flash('error'),
                ok: req.flash('info')}});
    }
});

router.post('/inicio_sesion',
        passport.authenticate('local-signin',
                {successRedirect: '/',
                    failureRedirect: '/',
                    failureFlash: true}
        ));

router.get('/cerrar_sesion', auth, cuentaC.cerrar_sesion);
//REGISTRO-MEDICO
router.get('/registro', function (req, res, next) {
    res.render('index', {title: 'Registrate', sesion: true, fragmento: "fragmentos/medico/frm_registro_medico"});
});
router.post('/registro', personaC.guardar);
//PACIENTE
router.get('/administracion/pacientes', pacienteC.visualizar);
router.post('/administracion/pacientes/guardar', pacienteC.guardar);
router.get('/administracion/pacientes/modificar/:external', pacienteC.visualizarModificar);
router.post('/administracion/pacientes/modificar/update', pacienteC.modificar);
//Buscar
router.get('/administracion/pacientes/buscar', pacienteC.buscar);
//Consulta
router.get('/administracion/consultas', auth, consultaC.visualizar);
router.post('/administracion/consultas/guardar', consultaC.guardar);
router.get('/administracion/consultas/modificar/:external', consultaC.visualizarModificar);
router.post('/administracion/consultas/modificar/guardar', consultaC.modificar);
//perfil
router.get('/administracion/fotos', fotoC.visualizar);
module.exports = router;
